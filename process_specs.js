function process_specs(geom_spec){
  geom_spec.meta = geom_spec.meta || {};
  geom_spec.meta.namespaceURI = 'http://www.w3.org/2000/svg';

  var props = geom_spec.props;
  var meta = geom_spec.meta;
  props['vector-effect'] = 'non-scaling-stroke';


  ////////
  // Extra properties
  if( props.w && ! props.width ){
    props.width = props.w;
  }
  if( props.h && ! props.height ){
    props.height = props.h;
  }
  if( props.cx && props.w && ! props.x ){
    props.x = props.cx - (props.w/2);
  }
  if( props.cy && props.h && ! props.y ){
    props.y = props.cy - (props.h/2);
  }

  if(props.rotated){
    props.transform = transform || '';
    props.transform += 'rotate(' + props.rotated + ' ' + props.x + ' ' + props.y + ')' ;
  }

  //////
  // Tag special cases
  if( geom_spec.tag === 'svg' ) {
    props.version = '1.1';
    props.xmlns = 'http://www.w3.org/2000/svg';
    props['xmlns:cc'] = 'http://creativecommons.org/ns#';
    props['xmlns:dc'] = 'http://purl.org/dc/elements/1.1/';
    props['xmlns:rdf'] = 'http://www.w3.org/1999/02/22-rdf-syntax-ns#';
    props['xmlns:xlink'] = 'http://www.w3.org/1999/xlink';
  } else if( geom_spec.tag === 'text' || geom_spec.tag === 'tspan' ) {
    if( props['stroke'] ){
      props.stroke = props.fill;
      props.fill = undefined;
    }
  } else if( geom_spec.tag === 'circ') {
    props['rx'] = props.d/2;
    props['ry'] = props.d/2;
    props['cx'] = x;
    props['cy'] = y;
  } else if( geom_spec.tag === 'ellipse') {
    props['rx'] = props.dx/2;
    props['ry'] = props.dy/2;
    props['cx'] = x;
    props['cy'] = y;
  } else if( geom_spec.tag === 'image') {
    props['width'] = props.w;
    props['height'] = props.h;
    props['xlink:href'] = props.href;
  }

  if( geom_spec.children && geom_spec.children.length >= 1 ) {
    geom_spec.children.forEach( function(child_spec){
      child_spec = process_specs(child_spec);
    });
  }

  return geom_spec;
}

module.exports = process_specs;
