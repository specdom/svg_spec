//var Table = require('./table');
//var mkSVG = require('./mkSVG');
var process_specs = require('./process_specs');

var svg_proto = {};

svg_proto.preset = function(name){ // set current preset
  if( typeof name === 'undefined' ){ // if no preset name given, reset to default
    this._preset_active = false;
  } else if ( ! (name in this._settings.preset_props) ) {
    console.warn('Error: unknown preset "'+name+'", using base');
    this._preset_active = 'base' ;
  } else { // finaly activate requested preset
    this._preset_active = name;
  }
  //*/
};

svg_proto.spec = function(spec) {
  spec.props = spec.props || {};
  spec.meta = spec.meta || {};
  spec.children = spec.children || [];
  this._drawing_parts.push(spec);
  return spec;
};

svg_proto._mkspec = function(tag, args) {
  var props;
  var children;
  var meta;
  var onchange;
  var inputs = [];
  args = Array.prototype.slice.call(args);
  args.forEach(function(argument){
    if(argument){
      if(argument.constructor === Number){
        argument = String(argument);
      }
      if(argument.constructor === Function){
        onchange = argument;
      } else if(argument.constructor === Array){
        children = argument;
      } else if(argument.constructor === Object){
        if( props === undefined ){
          props = argument;
        } else {
          meta = argument;
        }
      } else if(argument.constructor === String){
        inputs.push(argument);
      } else {
        console.warn('what do I do with a',argument.constructor,'?');
      }
    }
  });
  props = props || {};
  if( onchange ){ props.onchange = onchange; }
  meta = meta || {};

  if(meta.additional_properties){
    props = Object.assign({},meta.additional_properties,props);
  }

  if( this._preset_active ){
    if( ! meta.preset_name ) { meta.preset_name = this._preset_active; }
  }
  if( ! (meta.preset_name in this._settings.preset_props) ) {
    //console.warn('Error: preset "'+ meta.preset_name +'" name not found, using base. ', args );
    meta.preset_name = 'base';
  }

  var spec = {
    tag: tag,
    props: props,
    children: children,
    meta: meta,
  }

  /*
  if(this._group_active) {
    spec.meta.group_name = this._group_active;
    this._groups[this._group_active].children.push(spec);
  } else {
    this._drawing_parts.push(spec);
  }
  */
  if( inputs.length > 0 ){
    spec.text = inputs[0];

    if( tag === 'a'){
      spec.props = spec.props || {};
      spec.props.href = spec.props.href || inputs[1];
    }
    if( tag === 'img'){
      delete spec.text;
      spec.props = spec.props || {};
      spec.props.src = spec.props.src || inputs[0];
      spec.props.alt = spec.props.alt || inputs[1];
    }
    if( tag === 'image'){
      delete spec.text;
      spec.props = spec.props || {};
      spec.props['xlink:href'] = spec.props.src || inputs[0];
    }
  }

  var spec = process_specs(spec);
  return spec;
};

var cspec_replace = {
  '_': 'tag',
  '_c': 'children',
  '_t': 'text',
  '_m': 'meta',
  '_a': 'additional_properties',
  '_p': 'additional_properties',
};

svg_proto.compact_spec = function(cspec){
  Object.keys(cspec).forEach( (keyname)=>{
    if( cspec_replace[keyname] ){
      cspec[cspec_replace[keyname]] = cspec[keyname];
      delete cspec[keyname];
    }
  });
  var tag = cspec.tag;
  delete cspec.tag;
  var text = cspec.text;
  delete cspec.text;
  var children = cspec.children;
  delete cspec.children;
  var meta = cspec.meta;
  delete cspec.meta;
  if( cspec.additional_properties ){
    meta = meta || {}
    meta.additional_properties = cspec.additional_properties;
    delete cspec.additional_properties;
  };
  var props = cspec;
  if( tag.constructor === String ){
    if( children ){
      children.forEach( (child_cspec, i)=>{
        children[i] = this.compact_spec(child_cspec);
      });
    }
    var args = [
      text,
      props,
      children,
      meta,
    ];
    var spec = this._mkspec(tag,args);
    return spec;
  } else {
    console.log('cspec input not compatable:');
    console.log(tag,cspec,children)
    return false;
  }
};
svg_proto.compact_spec_list = function(lists){
  lists.forEach( (list) => {
    var spec = this.compact_spec(list);
    this._drawing_parts.push(spec);
  });
  return this.specs();
};

var SVG_tags = [
  'svg',
  'a',
  'animate',
  'animateMotion',
  'animateTransform',
  'circle',
  'clipPath',
  'ellipse',
  'defs',
  'desc',
  'discard',
  'g',
  'hatch',
  'hatchpath',
  'image',
  'line',
  'linearGradient',
  'marker',
  'mask',
  'metadata',
  'mpath',
  'path',
  'pattern',
  'polygon',
  'polyline',
  'radialGradient',
  'rect',
  'script',
  'set',
  'stop',
  'style',
  'switch',
  'symbol',
  'text',
  'textPath',
  'title',
  'tspan',
  'use',
  'view',
];


//var _mkspec

SVG_tags.forEach(function(tag){
  svg_proto[tag] = function(){
    var spec = this._mkspec(tag, arguments);
    if(this._group_active) {
      spec.meta.group_name = this._group_active;
      this._groups[this._group_active].children.push(spec);
    } else {
      this._drawing_parts.push(spec);
    }
    return this;
  };
  /*
  _mkspec[tag] = function(){
    return this._mkspec(tag, arguments);
  };
  */
});


svg_proto.group = function(name) {// set current group
  var x,y;
  if( arguments.length === 0 ){
    this._group_active = false;
    return this._group_active;
  } else if( arguments.length === 1 ){
    this._group_active = String(arguments[0]);
    if( this._groups[this._group_active] !== undefined ){
      console.log('group already exists, and is being appended');
    } else {
      this.g({
        id: name
      });
      if(this._group_active){
        this._groups[this._group_active] = this._last_element;
      }
    }
  } else { // if coor is passed
    var props = {};
    if( arguments[1].constructor === Object ){
      props = Object.assign(arguments[1])
    } else if( arguments.length === 2 ){
      if( typeof arguments[1].x !== 'undefined' ){
        props.x = arguments[1].x;
        props.y = arguments[1].y;
      } else {
        props.x = arguments[1][0];
        props.y = arguments[1][1];
      }
    } else if( arguments.length === 3 ){ // if x,y is passed
      props.x = arguments[1];
      props.y = arguments[2];
    }
    props.href = '#'+this._group_active;
    this.use(props);
    return this._last_element;
  }
};


svg_proto.append = function(svg){
  this._drawing_parts = this._drawing_parts.concat(svg._drawing_parts);
  return this;
};


svg_proto.specs = function(){
  var drawing = this;
  var width = drawing._settings.width;
  var height = drawing._settings.height;
  var x = drawing._settings.center.x - ( drawing._settings.viewbox_width/2 * drawing._settings.scale );
  var y = drawing._settings.center.y - ( drawing._settings.viewbox_height/2 * drawing._settings.scale );
  var w = drawing._settings.viewbox_width * drawing._settings.scale;
  var h = drawing._settings.viewbox_height * drawing._settings.scale;
  var view_box = x+' '+y+' '+w+' '+h;

  var svg_specs = this._mkspec('svg', [
    {
      viewBox: view_box,
      width,
      height,
    },
    //this._drawing_parts,
  ]);

  var specs = process_specs(svg_specs);
  specs.children = this._drawing_parts;
  return specs;
};

///////
// Defaults
var base_preset_props = {
  'fill': 'none',
  'stroke': '#000000',
  'stroke-width': '1px',
  'stroke-linecap': 'butt',
  'stroke-linejoin': 'miter',
  'stroke-opacity': 1,
  'font-family': 'monospace',
  'font-size': 10,
  'text-anchor': 'middle'
};





//export svg;
//export _mkspec;


export default function(settings){
  var svg = Object.create(svg_proto);
  svg._settings = settings || {};

  svg._settings.width = svg._settings.width || '100%';
  svg._settings.height = svg._settings.height || 'auto';
  svg._settings.viewbox_x = svg._settings.viewbox_x || 0;
  svg._settings.viewbox_y = svg._settings.viewbox_y || 0;
  svg._settings.viewbox_width = svg._settings.viewbox_width || 100;
  svg._settings.viewbox_height = svg._settings.viewbox_height || 100;
  svg._settings.scale = svg._settings.scale || 1;
  svg._settings.center = {
    x: svg._settings.viewbox_width / 2,
    y: svg._settings.viewbox_height / 2,
  };

  //svg._groups = svg._settings._groups || {};
  svg._groups = {};

  svg._settings.preset_props = svg._settings.preset_props || {
    base: base_preset_props,
  };
  svg._drawing_parts = [];
  svg._preset_active = false;
  //svg.presets = {};
  svg._group_active = false;
  svg._last_element = false;
  return svg;
};


